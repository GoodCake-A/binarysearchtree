using NUnit.Framework;
using BinarySearchTree.Tests.Comparers;

namespace BinarySearchTree.Tests
{
    public class BinaryTreeTests
    {
        private int[] inputSequence;

        private BinaryTree<int> tree;

        [SetUp]
        public void Setup()
        {
            inputSequence = new int[] { 4, 2, 6, 1, 3, 5, 7 };

            tree = new BinaryTree<int>(inputSequence);
        }

        [Test]
        public void AddTest()
        {
            foreach (var elem in inputSequence)
            {
                Assert.AreEqual(true, tree.Contains(elem));
            }
        }

        [Test]
        public void InorderTest()
        {
            var expected = new int[] { 1, 2, 3, 4, 5, 6, 7 };

            Assert.AreEqual(expected, tree.GetInorderEnummerable());
        }

        [Test]
        public void PreorderTest()
        {
            var expected = new int[] { 4, 2, 1, 3, 6, 5, 7 };

            Assert.AreEqual(expected, tree.GetPreorderEnummerable());
        }

        [Test]
        public void PostorderTest()
        {
            var expected = new int[] { 1, 3, 2, 5, 7, 6, 4 };

            Assert.AreEqual(expected, tree.GetPostorderEnummerable());
        }

        [Test]
        public void IntComparerTest()
        {
            var treeWithComparer = new BinaryTree<int>(this.inputSequence,new TestComparerInt());
            var expected = new int[] { 7, 6, 5, 4, 3, 2, 1 };

            Assert.AreEqual(expected, treeWithComparer.GetInorderEnummerable());
        }
    }
}