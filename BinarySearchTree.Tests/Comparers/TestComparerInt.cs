﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinarySearchTree.Tests.Comparers
{
    public class TestComparerInt : IComparer<int>
    {
        public int Compare(int x, int y)
        {
            if (x > y)
            {
                return -1;
            }

            if (x < y)
            {
                return 1;
            }

            return 0;
        }
    }
}
