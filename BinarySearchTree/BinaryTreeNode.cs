﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinarySearchTree
{
    public sealed class BinaryTreeNode<T>
    {
        private readonly T value;

        public BinaryTreeNode(T value, BinaryTreeNode<T> previousNode=null, BinaryTreeNode<T> leftNode=null, BinaryTreeNode<T> rightNode=null)
        {
            this.value = value;
            this.RightNode = rightNode;
            this.LeftNode = leftNode;
            this.PreviousNode = previousNode;
        }


        public T Value 
        { 
            get
            {
                return this.value;
            }
        }

        public BinaryTreeNode<T> LeftNode { get; set; }

        public BinaryTreeNode<T> RightNode { get; set; }

        public BinaryTreeNode<T> PreviousNode { get; set; }
    }
}
