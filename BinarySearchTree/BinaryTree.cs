﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinarySearchTree
{
    public class BinaryTree<T>
    {
        private readonly IComparer<T> comparer;

        private BinaryTreeNode<T> root;

        public BinaryTree()
        {
            this.comparer = Comparer<T>.Default;
        }

        public BinaryTree(IEnumerable<T> collection)
        {
            this.comparer = Comparer<T>.Default;

            if (collection is null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            foreach(var element in collection)
            {
                this.Add(element);
            }
        }

        public BinaryTree(IComparer<T> comparer)
        {
            this.comparer = comparer;

            if (comparer is null)
            {
                this.comparer = Comparer<T>.Default;
            }
            else
            {
                this.comparer = comparer;
            }
        }

        public BinaryTree(IEnumerable<T> collection, IComparer<T> comparer)
        {
            this.comparer = comparer;

            if (collection is null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            foreach (var element in collection)
            {
                this.Add(element);
            }

            this.comparer = comparer;
        }

        public bool Add(T value)
        {
            if (this.root is null)
            {
                this.root = new BinaryTreeNode<T>(value);
                return true;
            }

            return AddLeaf(root, value);
        }

        public bool Contains(T value)
        {
            if (this.root is null)
            {
                return false;
            }

            return FindElement(this.root, value);
        }

        public IEnumerable<T> GetPreorderEnummerable()
        {
            if (this.root is null)
            {
                yield break;
            }

            foreach (var element in EnumeratePreorder(this.root))
            {
                yield return element;
            }
        }

        public IEnumerable<T> GetInorderEnummerable()
        {
            if (this.root is null)
            {
                yield break;
            }

            foreach (var element in EnumerateInorder(this.root))
            {
                yield return element;
            }
        }

        public IEnumerable<T> GetPostorderEnummerable()
        {
            if (this.root is null)
            {
                yield break;
            }

            foreach (var element in EnumeratePostorder(this.root))
            {
                yield return element;
            }
        }

        private IEnumerable<T> EnumeratePreorder(BinaryTreeNode<T> root)
        {
            yield return root.Value;

            if (!(root.LeftNode is null))
            {
                foreach (var element in EnumeratePreorder(root.LeftNode))
                {
                    yield return element;
                }
            }

            if (!(root.RightNode is null))
            {
                foreach (var element in EnumeratePreorder(root.RightNode))
                {
                    yield return element;
                }
            }
        }

        private IEnumerable<T> EnumerateInorder(BinaryTreeNode<T> root)
        {
            if (!(root.LeftNode is null))
            {
                foreach (var element in EnumerateInorder(root.LeftNode))
                {
                    yield return element;
                }
            }

            yield return root.Value;

            if (!(root.RightNode is null))
            {
                foreach (var element in EnumerateInorder(root.RightNode))
                {
                    yield return element;
                }
            }
        }

        private IEnumerable<T> EnumeratePostorder(BinaryTreeNode<T> root)
        {
            if (!(root.LeftNode is null))
            {
                foreach (var element in EnumeratePostorder(root.LeftNode))
                {
                    yield return element;
                }
            }

            if (!(root.RightNode is null))
            {
                foreach (var element in EnumeratePostorder(root.RightNode))
                {
                    yield return element;
                }
            }

            yield return root.Value;
        }

        private bool FindElement(BinaryTreeNode<T> root, T value)
        {
            int comparsionResult = this.comparer.Compare(root.Value, value);

            if (comparsionResult < 0)
            {
                if (root.RightNode is null)
                {
                    return false;
                }

                return FindElement(root.RightNode, value);
            }

            if (comparsionResult > 0)
            {
                if (root.LeftNode is null)
                {
                    return false;
                }

                return FindElement(root.LeftNode, value);
            }

            return true;
        }

        private bool AddLeaf(BinaryTreeNode<T> root, T leafValue)
        {
            int comparsionResult = this.comparer.Compare(root.Value, leafValue);

            if (comparsionResult < 0)
            {
                if (root.RightNode is null)
                {
                    root.RightNode = new BinaryTreeNode<T>(leafValue, root);
                    return true;
                }

                return AddLeaf(root.RightNode, leafValue);
            }

            if (comparsionResult > 0)
            {
                if (root.LeftNode is null)
                {
                    root.LeftNode = new BinaryTreeNode<T>(leafValue, root);
                    return true;
                }

                return AddLeaf(root.LeftNode, leafValue);
            }

            return false;
        }
    }
}
